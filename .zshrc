export ZSH=$HOME/.oh-my-zsh

ZSH_THEME="robbyrussell"

# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

CASE_SENSITIVE="true"
export UPDATE_ZSH_DAYS=7
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
HIST_STAMPS="yyyy-mm-dd"

plugins=(
  z
  wd
  pip
  git
  sudo
  ruby
  rails
  extract
  textmate
  lighthouse
  zsh-autosuggestions
  zsh-syntax-highlighting
)

source $ZSH/oh-my-zsh.sh
export MANPATH="/usr/local/man:$MANPATH"
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
export ARCHFLAGS="-arch x86_64"

alias zshconfig="nvim ~/.zshrc"
alias vim="nvim"
alias cat="bat"
alias c="clear"
alias ls="exa"
alias l="exa -l"
alias n="neofetch"
alias lg="lazygit"
alias gl="git log --oneline"
